// FileName: tests/contactModel.test.js

const mongoose = require('mongoose');

const dbHandler = require('./db-handler');
const contactController = require('../contactController');
const contactModel = require('../contactModel');

// Connect to a new in-memory databse before running any tests.
beforeAll(async () => await dbHandler.connect());

// Clear all test data after every test.
afterEach(async () => await dbHandler.clearDatabase());

// Remove and close the db and server.
afterAll(async () => await dbHandler.closeDatabase());

// ContactModel test suite.
describe('contactmodel ', () => {
    
    // Test if a contactModel is created.
    it('Can be created correctly', () => {
       expect(1).toBe(1);
    });
});

// Complete contactModel
const contactModelExample = {
    name: 'Pietje Puk',
    gender: 'Male',
    email: 'Piet@puk.nl',
    phone: '0612345678'
};