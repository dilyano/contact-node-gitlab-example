// FileName contactController.js

// Import contact model
Contact = require('./contactModel');

// Handle index actions
exports.index = function(request, response) {
  Contact.get(function(error, contacts) {
    if (error) {
      response.json({
        status: 'error',
        message: error,
      });
    }
    response.json({
      status: 'success',
      message: 'Contacts retrieved successfully',
      data: contacts,
    });
  });
};

// Handle create contat actions
exports.new = function(request, response) {
  const contact = new Contact();
  contact.name = request.body.name ? request.body.name : contact.name;
  contact.gender = request.body.gender;
  contact.email = request.body.email;
  contact.phone = request.body.phone;

  // Save the contact and check for errors
  contact.save(function(error) {
    if (error) {
      response.json(error);
    }

    response.json({
      message: 'New contact created!',
      data: contact,
    });
  });
};

// Handle view contact info
exports.view = function(request, response) {
  Contact.findById(request.params. contact_id, function(error, contact) {
    if (error) {
      response.send(error);
    }
    response.json({
      message: 'Contact details loading..',
      data: contact,
    });
  });
};

// Handel update contact info
exports.update = function(request, response) {
  Contact.findById(request.params.contact_id, function(error, contact) {
    if (error) {
      response.send(error);
    }

    contact.name = request.body.name ? request.body.name : contact.name;
    contact.gender = request.body.gender;
    contact.email = request.body.email;
    contact.phone = request.body.phone;

    // Save the contact and check for errors
    contact.save(function(eror) {
      if (error) {
        response.json(error);
      }
      response.json({
        message: 'Contact Info updated',
        data: contact,
      });
    });
  });
};

// Handle delete contact
exports.delete = function(request, response) {
  Contact.remove({
    _id: request.params.contact_id,
  }, function(error, contact) {
    if (error) {
      response.send(error);
    }
    response.json({
      status: 'success',
      message: 'Contact deleted',
    });
  });
};
